-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-09-2017 a las 14:27:21
-- Versión del servidor: 5.5.31
-- Versión de PHP: 5.4.45-0+deb7u7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `femdevs-dialogo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dialogos`
--

CREATE TABLE IF NOT EXISTS `dialogos` (
  `dialogo_id` int(11) NOT NULL AUTO_INCREMENT,
  `dialogo_titulo` varchar(255) NOT NULL,
  `dialogo_texto` text NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dialogo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `dialogos`
--

INSERT INTO `dialogos` (`dialogo_id`, `dialogo_titulo`, `dialogo_texto`, `borrado`) VALUES
(1, 'Dialogo de prueba', 'Test de dialogo hecho por monica_team\r\n', 0),
(2, 'Dialogo seguido', 'Este dialogo viene del 1, y vuelve para el ID 1.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dialogos_botones`
--

CREATE TABLE IF NOT EXISTS `dialogos_botones` (
  `boton_id` int(11) NOT NULL AUTO_INCREMENT,
  `dialogo_id` int(11) NOT NULL,
  `boton_texto` varchar(128) NOT NULL,
  `boton_dialogo` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`boton_id`),
  KEY `dialogo_id` (`dialogo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `dialogos_botones`
--

INSERT INTO `dialogos_botones` (`boton_id`, `dialogo_id`, `boton_texto`, `boton_dialogo`, `borrado`) VALUES
(1, 1, 'Adelante', 2, 0),
(2, 1, 'Cancelar', -1, 0),
(3, 2, 'Atras', 1, 0),
(4, 2, 'Cancelar', -1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
