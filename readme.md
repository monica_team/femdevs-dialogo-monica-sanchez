# README #

Este proyecto es para el reto de femcode de femdevs. Consiste es un sistema de dialogos, los dialogos aparecen en el lado de la izquierda. Y para poder modificarlos debes ir donde pone Gestor dialogos.

### Como se configura? ###

* Debes importar el archivo .sql que esta en el root (/) del proyecto
* Y luego configura el acceso a la base de datos que esta dentro de: core/sistema/mysql_class.php

### Probar el sistema ###
* Puedes probar el sistema en: http://apps.tecnomakers.net/femdevs-monica-team/public_html/
* Recuerda que la live demo que existe, cualquiera la puede modificar, por lo tanto, no te extrañes si alguien modifica al mismo tiempo que tu.