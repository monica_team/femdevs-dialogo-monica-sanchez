<?php
require("../core/core.php");
?>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="Reto de femdevs, de programar sistema de dialogos">
	    <meta name="author" content="Monica Team">
	    <title>Femcode -> Dialogo | monica_team </title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
	    <script src="js/jquery-1.10.2.js"></script>
	    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
		<script src="js/dialogos.js"></script>
	</head>
	<body role="document">

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse">
     	<div class="container">
        	<div class="navbar-header">
          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="index.php">Dialogo</a>
        	</div>
	        <div id="navbar" class="navbar-collapse navbar-left collapse">
	            <ul class="nav navbar-nav">
	                <li><a href="index.php">Inicio</a></li>
	                <li><a id="panelDialogo" href="javascript:void(0);">Gestor dialogos</a></li>
	            </ul>
	        </div><!--/.nav-collapse -->
      	</div>
    </nav>

    <div class="container theme-showcase" role="main">
    	<?php
        require($directorio."/controladores/controlador_vistas.php");
        ?>
    </div> <!-- /container -->
</html>