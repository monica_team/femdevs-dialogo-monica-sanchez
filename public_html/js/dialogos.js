(function( $ ){
	$.fn.cargarDialogo = function(dialogoID) {
		$("#ventanaDialogo").modal('show');
		$("#ventanaDialogo .modal-body").html("<img src='imagenes/ajax-loader.gif'/>");
		$("#ventanaDialogo .modal-footer").html("");
		$.post("index.php?modulo=ControladorDialogo&accion=infoDialogo&id="+dialogoID, function(dataServer) {
			var Respuesta = JSON.parse(dataServer);
			var htmlBotones = "";
			$("#ventanaDialogo .modal-title").html(Respuesta.dialogoTitulo);
			$("#ventanaDialogo .modal-body").html(Respuesta.dialogoTexto);
			$.each(Respuesta.dialogoOpciones, function( index, obj ) {
				htmlBotones += "<button type='button' class='btn btn-primary gestorBotones' boton_id='"+obj.boton_id+"' dialogo_id='"+obj.boton_dialogo+"'>"+obj.boton_texto+"</button>";
			});
			$("#ventanaDialogo .modal-footer").html(htmlBotones);
		});
	};

	$.fn.cargarBoton = function(dialogoID,botonID) {
		if(parseInt(dialogoID) > 0) {
			$.post("index.php?modulo=ControladorBotones&accion=InfoBoton&id="+botonID, function(dataServer) {
				var Respuesta = JSON.parse(dataServer);
				if(parseInt(Respuesta.boton_dialogo) == parseInt(dialogoID)) {
					$(this).cargarDialogo(dialogoID);
				}
			});
		} else {
			$("#ventanaDialogo").modal('hide');
		}
	};

	$.fn.cargarGestorDialogo = function() {
		$("#gestorDialogo").modal('show');
		$("#ladoEdicion").hide();
		$("#ventanaDialogo .modal-body").html("<img src='imagenes/ajax-loader.gif'/>");
		
		$.post("index.php?modulo=ControladorDialogo&accion=cargarDialogo", function(dataServer) {
			var Respuesta = JSON.parse(dataServer);
			var htmlTabla = "<table class='table table-striped'><thead><tr><th>Dialogo</th></tr></thead><tbody>"; 
            			
            		
			$.each(Respuesta, function( index, obj ) {
				htmlTabla += "<tr class='editarDialogo' dialogo_id='"+obj.dialogoID+"'><td>"+obj.dialogoTitulo+"</td></tr>";
			});

			htmlTabla += "</tbody></table>";
			$("#tablaDialogos").html(htmlTabla);
		});
	};


	//---------abrir ventana de edicion------------
	$.fn.editarDialogo = function(dialogoID) {
		$("#ladoEdicion").show();
		
		if(parseInt(dialogoID) > 0) {
			$("#ladoEdicion .cargando").show();
			$.post("index.php?modulo=ControladorDialogo&accion=infoDialogo&id="+dialogoID, function(dataServer) {
				var Respuesta = JSON.parse(dataServer);

				$("#tituloDelDialogo").val(Respuesta.dialogoTitulo);
				$("#textoDelDialogo").val(Respuesta.dialogoTexto);
				$(".botonesModificar").attr("dialogo_id",dialogoID);

				var htmlDeBotones = "";
				$.each(Respuesta.dialogoOpciones, function( index, obj ) {
					htmlDeBotones += "<a href='javascript:void(0);' class='list-group-item gestionBoton' boton_id='"+obj.boton_id+"'>"+obj.boton_texto+"</a>";
				});

				$("#listadoBotones").html(htmlDeBotones);

				$(".borrarDialogo").attr("dialogo_id",dialogoID);
				$(".guardarDialogo").attr("dialogo_id",dialogoID);
				$("#editorBotones").show();
				$("#lateralEditarBotones").hide();
				$(".borrarDialogo").show();
			});
			
		} else {
			$("#lateralEditarBotones").hide();
			$("#editorBotones").hide();
			$(".guardarDialogo").attr("dialogo_id",0);
			$(".borrarDialogo").hide();
			$("#tituloDelDialogo").val("");
			$("#textoDelDialogo").val("");
		}
	};

	$.fn.recargarBotones = function(dialogoID) {
		$.post("index.php?modulo=ControladorDialogo&accion=infoDialogo&id="+dialogoID, function(dataServer) {
			var Respuesta = JSON.parse(dataServer);

			var htmlDeBotones = "";
			$.each(Respuesta.dialogoOpciones, function( index, obj ) {
				htmlDeBotones += "<a href='javascript:void(0);' class='list-group-item gestionBoton' boton_id='"+obj.boton_id+"'>"+obj.boton_texto+"</a>";
			});

			$("#listadoBotones").html(htmlDeBotones);
		});
	};

	//---Funcion de borrar el dialogo.-----
	$.fn.borrarElDialogo = function(dialogoID) {
		if(parseInt(dialogoID) > 0) {
			$.post("index.php?modulo=ControladorDialogo&accion=borrarDialogo&id="+dialogoID, function(dataServer) {
				$(this).cargarGestorDialogo();
				$(this).recargarDialogos();
			});
		}
	};

	//----Funcion de guardar el dialogo (guardar los datos en el server)-------
	$.fn.GuardarElDialogo = function (dialogoID) {
		$.post("index.php?modulo=ControladorDialogo&accion=gestionDialogo&id="+dialogoID, { dialogoTitulo:$("#tituloDelDialogo").val(), dialogoTexto:$("#textoDelDialogo").val() } , function(dataServer) {
			$(this).cargarGestorDialogo();
			$(this).recargarDialogos();
		});
	};


	//-----------Abrir lateral de editar boton---
	$.fn.editarBoton = function(botonID) {
		$("#lateralEditarBotones").show();
		if(parseInt(botonID) > 0) {
			$.post("index.php?modulo=ControladorBotones&accion=InfoBoton&id="+botonID, function(dataServer) {
				var Respuesta = JSON.parse(dataServer);
				$("#textoDelBoton").val(Respuesta.boton_texto);
				$("#valorDialogo").val(Respuesta.boton_dialogo);
				if(parseInt(Respuesta.boton_dialogo) > 0) {
					$("#dialogoConectado").html(Respuesta.boton_dialogo_txt);
				} else {
					$("#dialogoConectado").html("Ninguno");
				}
				$(".botonesModificar").attr("boton_id",botonID);
			});
		} else {
			$("#textoDelBoton").val("");
			$("#valorDialogo").val(-1);
			$("#dialogoConectado").html("");
			$(".botonesModificar").attr("boton_id",0);
		}
	};


	//----Guardar el boton-----
	$.fn.guardarBoton = function(botonID,dialogo_id) {
		if(!parseInt(botonID)) {
			botonID = -1;
		}
		$.post("index.php?modulo=ControladorBotones&accion=gestionBoton&id="+botonID, { dialogoID:dialogo_id,boton_texto:$("#textoDelBoton").val(),boton_dialogo:$("#valorDialogo").val()  } , function(dataServer) {
			$(this).recargarBotones(dialogo_id);
			$("#lateralEditarBotones").hide();
		});
		
	};

	$.fn.borrarBoton = function(botonID,dialogo_id) {
		$.post("index.php?modulo=ControladorBotones&accion=BorrarBoton&id="+botonID, function(dataServer) {
			$(this).recargarBotones(dialogo_id);
			$("#lateralEditarBotones").hide();
		});
		
	}


	//-----Recargar lista de dialogos-----
	$.fn.recargarDialogos = function() {
		$.post("index.php?modulo=ControladorDialogo&accion=cargarDialogo", function(dataServer) {
			var Respuesta = JSON.parse(dataServer);
			var htmlTabla = '';
            var htmlConexion = '<a href="javascript:void(0);" class="list-group-item conexionDialogo" dialogoID="-1">Sin conectar</a>';

			$.each(Respuesta, function( index, obj ) {
				var htmlActual = "<a href='javascript:void(0);' class='list-group-item accederDialogo' dialogoID='"+obj.dialogoID+"'>"+obj.dialogoTitulo+"</a>";
				htmlTabla += htmlActual;
				htmlConexion += htmlActual;
			});

			$("#listaDeLosDialogos").html(htmlTabla);
			$("#listadoConexion").html(htmlConexion);
		});
	};


})( jQuery );


//-------INICIO GESTION DE DIALOGOS---------
$(document).on('click', '.accederDialogo', function() {
	var DialogoID = $(this).attr("dialogoID");
	$(this).cargarDialogo(DialogoID);
});

$(document).on('click', '.gestorBotones', function() {
	var DialogoID = $(this).attr("dialogo_id");
	var botonID = $(this).attr("boton_id");
	$(this).cargarBoton(DialogoID,botonID);
});

$(document).on('click','#panelDialogo', function() {
	$(this).cargarGestorDialogo();
});

$(document).on('click','.editarDialogo', function() {
	var DialogoID = $(this).attr("dialogo_id");
	$(this).editarDialogo(DialogoID);
});



$(document).on('click','.guardarDialogo', function() {
	var DialogoID = $(this).attr("dialogo_id");
	$(this).GuardarElDialogo(DialogoID);
});

$(document).on('click','.borrarDialogo', function() {
	var DialogoID = $(this).attr("dialogo_id");
	$(this).borrarElDialogo(DialogoID);
});
//-------FIN DE GESTION DE DIALOGOS-------

//-------INICIO GESTION DE BOTONES---------
$(document).on('click','.gestionBoton', function() {
	var idBoton = $(this).attr("boton_id");
	$(".gestionBoton").removeClass("active");
	$(this).addClass("active");
	$(this).editarBoton(idBoton);
});

$(document).on('click','.selectorDialogo', function() {
	$("#listadoDeDialogos").modal('show');
});

$(document).on('click','.conexionDialogo', function() {
	var seleccionadoID = $(this).attr("dialogoID");
	var seleccionadoTXT = $(this).html();
	$("#valorDialogo").val(seleccionadoID);
	$("#dialogoConectado").html(seleccionadoTXT);
	$("#listadoDeDialogos").modal('hide');
});

$(document).on('click','.guardarBoton', function() {
	var botonID = $(this).attr("boton_id");
	var dialogo_id = $(this).attr("dialogo_id");
	$(this).guardarBoton(botonID,dialogo_id);
});

$(document).on('click','.borrarBoton', function() {
	var botonID = $(this).attr("boton_id");
	var dialogo_id = $(this).attr("dialogo_id");
	$(this).borrarBoton(botonID);
});
//-------FIN DE GESTION DE BOTONES-------