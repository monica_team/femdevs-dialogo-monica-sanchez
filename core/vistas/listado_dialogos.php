<?php
//---Carga base---
$dialogos = new Dialogos(0);
$listadoDialogos = $dialogos->getDialog();
?>
<div id="ventanaDialogo" class="modal fade modal-primary" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cargando....</h4>
        </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
            
        </div>
    </div>

    </div>
</div>

<div id="gestorDialogo" class="modal fade modal-primary" role="dialog">
    <div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Gestor de dialogos</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<a class="btn btn-primary pull-right editarDialogo" dialogo_id="0"><i class="glyphicon glyphicon-pencil"></i> Nuevo dialogo </a>
        	</div>
        	<div class="row">
	            <div class="col-md-3" id="tablaDialogos">
	            	
	            </div>
	            <div class="col-md-9 cargando" style="display:none;">
	            	<img src='imagenes/ajax-loader.gif' />
	            </div>
	            <div class="col-md-9" id="ladoEdicion" style="display:none;">
	            	<div class="row">
	            		<h2><strong>Editor</strong></h2>
	            	</div>
	            	<div class="row">
		            	<div class="col-md-4">
		            		<strong>Titulo</strong>
		            	</div>
		            	<div class="col-md-4">
		            		<input type="text" class="form-control" id="tituloDelDialogo" value=""/>
		            	</div>
		            </div>
		            <div class="row">
		            	<div class="col-md-12">
		            		<strong>Texto del dialogo</strong>
		            	</div>
		            	<div class="col-md-12">
		            		<input type="text" class="form-control" id="textoDelDialogo" value=""/>
		            	</div>
		            </div>

		            <div class="row" id="editorBotones" style="display:none;">
		            	<div class="col-md-12">
		            		<div class="panel panel-primary"> 
		            			<div class="panel-heading"> 
		            				<h3 class="panel-title">Botones <a class="btn btn-primary pull-right gestionBoton btn-xs" dialogo_id="0" boton_id="-1"><i class="glyphicon glyphicon-pencil"></i> Nuevo boton </a></h3> 
		            			</div> 
		            			<div class="panel-body">
		            				
									<div class="row">
										<div class="col-md-4">
											<div class="list-group" id="listadoBotones">
												
											</div>
										</div>
										<div class="col-md-8" id="lateralEditarBotones" style="display:none;">
											<div class="row">
												<div class="col-md-4">
													Texto
												</div>
												<div class="col-md-8">
													<input type="text" id="textoDelBoton" value=""/>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													¿Dialogo conectado?
												</div>
												<div class="col-md-8">
													<input type="hidden" id="valorDialogo" value="-1"/>
													<a href="javascript:void(0);" class="selectorDialogo btn btn-primary">Seleccionar dialogo</a>
													<span id="dialogoConectado"></span>
												</div>
											</div>
											<div class="row">
												<a class="btn btn-primary pull-right guardarBoton botonesModificar" dialogo_id="0" boton_id="0"><i class="glyphicon glyphicon-download-alt"></i> Guardar boton </a>
												<a class="btn btn-primary pull-right borrarBoton botonesModificar" dialogo_id="0" boton_id="0"><i class="glyphicon glyphicon-trash"></i> Borrar boton </a>
											</div>
										</div>
									</div>
		            			</div> 
		            		</div>
		            	</div>
		            </div>

		            <div class="row">
		            	<div class="col-md-12">
		            		<a class="btn btn-primary pull-right guardarDialogo" dialogo_id="0"><i class="glyphicon glyphicon-download-alt"></i> Guardar dialogo </a>
		            		<a class="btn btn-primary pull-right borrarDialogo" dialogo_id="0"><i class="glyphicon glyphicon-trash"></i> Borrar dialogo </a>
		            	</div>
		            </div>
		        </div>
            </div>
        </div>
        <div class="modal-footer">
            
        </div>
    </div>

    </div>
</div>


<div id="listadoDeDialogos" class="modal fade modal-primary" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Selecciona el dialogo que quieres conectar</h4>
        </div>
        <div class="modal-body">
        	<div class="list-group" id="listadoConexion">
	        	<a href="javascript:void(0);" class="list-group-item conexionDialogo" dialogoID="-1">Sin conectar</a>
	            <?php
				foreach ($listadoDialogos as $dialogo) {
					echo '<a href="javascript:void(0);" class="list-group-item conexionDialogo" dialogoID="'.$dialogo->dialogoID.'">'.$dialogo->dialogoTitulo.'</a>';
				}
				?>
			</div>
        </div>
        <div class="modal-footer">
		    <a class="btn btn-primary" data-dismiss="modal"><i class="glyphicon glyphicon-trash"></i> Cancelar </a>
        </div>
    </div>

    </div>
</div>


<div class="col-md-4">
	<div class="panel panel-primary"> 
		<div class="panel-heading"> 
			<h3 class="panel-title">Listado de dialogos</h3> 
		</div> 
		<div class="panel-body">	
			<div class="list-group" id="listaDeLosDialogos">
				<?php
				foreach ($listadoDialogos as $dialogo) {
					echo '<a href="javascript:void(0);" class="list-group-item accederDialogo" dialogoID="'.$dialogo->dialogoID.'">'.$dialogo->dialogoTitulo.'</a>';
				}
				?>
			</div>
		</div> 
	</div>

</div>

<div class="col-md-8">
	<div class="jumbotron">
	  	<h1>Femdevs - Sistema dialogo</h1>
	  	<p>Esta app web de gestion de dialogos ha sido creado por monica_team, puedes obtener el codigo fuente con una base de datos con datos de pruebas en el boton de abajo</p>
	  	<p><a class="btn btn-primary btn-lg" target="_blank" href="https://bitbucket.org/monica_team/femdevs-dialogo-monica-sanchez" role="button">Bitbucket</a></p>
	</div>
</div>