<?php
/*
Clase de gestion de los botones de los dialogos
Autora: monica_team



*/
class ControladorBotones {

	function gestionBoton($botonID) {
		if(isset($_POST['dialogoID']) && $_POST['dialogoID'] > 0) {
			$dialogo_id = $_POST['dialogoID'];
			//----gestion----
			if($botonID == -1) {
				$boton = new Dialogos_botones($dialogo_id,0);
			} else {
				$boton = new Dialogos_botones(0,$botonID);
				$boton->getBoton();
			}
			$boton->boton_texto = $_POST['boton_texto'];
			$boton->boton_dialogo = $_POST['boton_dialogo'];
			$boton->guardarBoton();	
		}
		
	}

	function BorrarBoton($botonID) {
		if($botonID > 0) {
			$botones = new Dialogos_botones(0,$botonID);
			$botones->borrarBoton();
		}
	}

	function InfoBoton($botonID) {
		$Boton = new Dialogos_botones(0,$botonID);
		$Boton->getBoton();
		echo json_encode($Boton, JSON_UNESCAPED_UNICODE);
	}
}


?>