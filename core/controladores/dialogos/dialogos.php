<?php
/*
Clase de gestion de los dialogos
Autora: monica_team



*/
class ControladorDialogo {
	//Ejemplo de uso: 
	//http://apps.tecnomakers.net/femdevs-monica-team/public_html/?modulo=ControladorDialogo&accion=cargarDialogo
	function cargarDialogo() {
		$dialogos = new Dialogos(0);
		echo json_encode($dialogos->getDialog(), JSON_UNESCAPED_UNICODE);
	}

	function borrarDialogo($dialogoID) {
		if($dialogoID > 0) {
			$dialogos = new Dialogos($dialogoID);
			$dialogos->borrarDialogo();
		}
	}

	function gestionDialogo($dialogoID) {
		if($dialogoID == -1) {
			$dialogos = new Dialogos(0);
		} else {
			$dialogos = new Dialogos($dialogoID);
			$dialogos->getDialog();
		}
		$dialogos->dialogoTitulo = $_POST['dialogoTitulo'];
		$dialogos->dialogoTexto = $_POST['dialogoTexto'];
		$dialogos->setDialog();
	}

	function infoDialogo($dialogoID) {
		$dialogos = new Dialogos($dialogoID);
		$dialogos->getDialog();
		echo json_encode($dialogos, JSON_UNESCAPED_UNICODE);
	}
}

?>