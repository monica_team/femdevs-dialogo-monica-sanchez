<?php

class Dialogos_botones {
	private $dbh;

	var $boton_id;
	var $dialogo_id;
	var $boton_texto="";
	var $boton_dialogo;
	var $boton_dialogo_txt= "";

	function __construct($dialogoID,$botonID=0) {
		$this->dbh = MySQL_Server::getInstance();
		$this->boton_id = $botonID;
		$this->dialogo_id = $dialogoID;
	}

	function getBoton() {
		if($this->boton_id > 0 && $this->dialogo_id == 0) {
			$qBoton = $this->dbh->Consulta("SELECT btxt.dialogo_id,btxt.boton_texto,btxt.boton_dialogo,IF(btxt.boton_dialogo > 0,(SELECT dialogo_titulo FROM dialogos WHERE dialogo_id=btxt.boton_dialogo), '') AS boton_dialogo_txt FROM dialogos_botones btxt WHERE btxt.boton_id='".$this->boton_id."' AND btxt.borrado='0' ");
			$infoBoton = $qBoton->fetch_assoc();

			$this->dialogo_id = $infoBoton['dialogo_id'];
			$this->boton_texto = $infoBoton['boton_texto'];
			$this->boton_dialogo = $infoBoton['boton_dialogo'];
			$this->boton_dialogo_txt = $infoBoton['boton_dialogo_txt'];

		} else if($this->dialogo_id > 0 && $this->boton_id == 0) {

			$qBoton = $this->dbh->Consulta("SELECT boton_id FROM dialogos_botones WHERE dialogo_id='".$this->dialogo_id."' AND borrado='0' ");
			$arrayBotones = array();
			while($infoBoton = $qBoton->fetch_assoc()) {
				$tmpBoton = new Dialogos_botones(0,$infoBoton['boton_id']);
				$tmpBoton->getBoton();
				array_push($arrayBotones, $tmpBoton);
			}

			return $arrayBotones;
		}
	}

	function guardarBoton() {
		//Actualizar
		if($this->boton_id > 0) {
			$this->dbh->Consulta("UPDATE dialogos_botones SET boton_texto='".$this->dbh->real_escape_string($this->boton_texto)."',boton_dialogo='".$this->boton_dialogo."' WHERE boton_id='".$this->boton_id."' LIMIT 1");
		} else {
			$this->dbh->Consulta("INSERT INTO dialogos_botones (dialogo_id,boton_texto,boton_dialogo) VALUES ('".$this->dialogo_id."','".$this->dbh->real_escape_string($this->boton_texto)."','".$this->boton_dialogo."') ");
			$this->boton_id = $this->dbh->id();
		}
	}

	function borrarBoton() {
		if($this->boton_id > 0) {
			$this->dbh->Consulta("UPDATE dialogos_botones SET borrado='1' WHERE boton_id='".$this->boton_id."' LIMIT 1");
		}
	}
}

?>