<?php


class Dialogos {
	private $dbh;

	public $dialogoID;
	public $dialogoTitulo="";
	public $dialogoOpciones=array();
	public $dialogoTexto="";

	function __construct($dialogo_id) {
		$this->dbh = MySQL_Server::getInstance();
		$this->dialogoID = $dialogo_id;
	}

	function getDialog() {
		if($this->dialogoID > 0) {
			$qDialogo = $this->dbh->Consulta("SELECT dialogo_titulo,dialogo_texto FROM dialogos WHERE dialogo_id='".$this->dialogoID."' AND borrado='0' ");
			$infoDialogo = $qDialogo->fetch_assoc();

			$this->dialogoTitulo = $infoDialogo['dialogo_titulo'];
			$this->dialogoTexto = $infoDialogo['dialogo_texto'];
			$this->dialogoOpciones = $this->ObtenerOpciones();
		} else if($this->dialogoID == 0) {
			$arrayDialogos = array();
			$qDialogo = $this->dbh->Consulta("SELECT dialogo_id FROM dialogos WHERE borrado='0' ");
			while($infoDialogo = $qDialogo->fetch_assoc()) {
				$tmpDialogo = new Dialogos($infoDialogo['dialogo_id']);
				$tmpDialogo->getDialog();
				
				array_push($arrayDialogos, $tmpDialogo);
			}
			return $arrayDialogos;
		}
	}

	function setDialog() {
		if($this->dialogoID > 0) {
			$this->dbh->Consulta("UPDATE dialogos SET dialogo_titulo='".$this->dbh->real_escape_string($this->dialogoTitulo)."',dialogo_texto='".$this->dbh->real_escape_string($this->dialogoTexto)."' WHERE dialogo_id='".$this->dialogoID."' LIMIT 1");
		} else {
			$this->dbh->Consulta("INSERT INTO dialogos (dialogo_titulo,dialogo_texto) VALUES ('".$this->dbh->real_escape_string($this->dialogoTitulo)."','".$this->dbh->real_escape_string($this->dialogoTexto)."') ");
			$this->dialogoID = $this->dbh->id();
		}
	}

	function ObtenerOpciones() {
		if($this->dialogoID > 0) {
			$listadoOpciones = new Dialogos_botones($this->dialogoID,0);
			return $listadoOpciones->getBoton();
		}
	}

	function borrarDialogo() {
		if($this->dialogoID > 0) {
			$this->dbh->Consulta("UPDATE dialogos SET borrado='1' WHERE dialogo_id = '".$this->dialogoID."'");
		}
	}
}


?>