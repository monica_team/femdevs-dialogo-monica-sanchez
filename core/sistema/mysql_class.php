<?php

class MySQL_Server {
	var $conexion;
    private static $instance;

	function __construct() {
		$this->conexion = new mysqli();
		@$this->conexion->connect("IP", "femdevs-dialogo", "pass", "femdevs-dialogo");
	}
    
    private function __clone() {}

    public static function getInstance() {
        if (!MySQL_Server::$instance instanceof self) {
             MySQL_Server::$instance = new self();
        }
        return MySQL_Server::$instance;
    }

    public function Consulta($query) {
       return $this->conexion->query($query);
    }

    public function id() {
        return $this->conexion->insert_id;
    }

    public function real_escape_string($valor) {
        return $this->conexion->real_escape_string($valor);

    }

}


?>